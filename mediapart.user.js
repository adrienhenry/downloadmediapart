// ==UserScript==
// @name        Mediapart Download epub
// @namespace   GuGuss
// @description Download a mediapart article in an epub format.
// @include     https://*.mediapart.fr/*
// @version     0.0
// @updateURL   https://adrienhenry@bitbucket.org/adrienhenry/downloadmediapart.git
// @grant       GM_xmlhttpRequest
// @grant       GM_setValue
// @grant       GM_getValue
// @icon        https://blogs.mediapart.fr/images/apple-touch-icon.png
// ==/UserScript==



var articleText = document.getElementsByClassName("content-article")
var articleTitle = document.getElementsByClassName("title")

/* --- MAIN SCRIPT ENTRY --- */
main();
function main() {
    var button = document.createElement('a');
    console.log("Hello")
}
